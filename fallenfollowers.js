Users = new Mongo.Collection('users');
var clientid = 'a2b3e50d5fab459fc99da2a2c826cb75';
var views = {};
if (Meteor.isClient) {

  // Init Vars
  Session.set('followers',null);
  Session.set('tags',[]);
  var dev = ("localhost" === window.location.hostname)||false;
  window.activesession = Session.get('userobj')||null;

  // ROUTES

  Router.route('/',function(){
    this.render('Login');
    console.log(0);
  });

  Router.route('/home',function(){
    if(!activesession){
      Router.go('/');
      return;
    }
    this.render('Home');
    console.log(1);
  });

  Router.route('/about',function(){
    if(!activesession){
      Router.go('/');
      return;
    }
    this.render('About');
  });


  function loguser(userobj){
    if(dev)
      userobj = {"id":8891244,"kind":"user","permalink":"user3845270","username":"Jabran Saeed","last_modified":"2015/10/10 15:50:53 +0000","uri":"https://api.soundcloud.com/users/8891244","permalink_url":"http://soundcloud.com/user3845270","avatar_url":"https://i1.sndcdn.com/avatars-000154445748-r28pgk-large.jpg","country":"Pakistan","first_name":"Jabran","last_name":"Saeed","full_name":"Jabran Saeed","description":"canvas of the air.","city":"Lahore","discogs_name":null,"myspace_name":null,"website":null,"website_title":null,"online":false,"track_count":7,"playlist_count":1,"plan":"Free","public_favorites_count":172,"followers_count":277,"followings_count":96,"subscriptions":[],"upload_seconds_left":8314,"quota":{"unlimited_upload_quota":false,"upload_seconds_used":2485,"upload_seconds_left":8314},"private_tracks_count":9,"private_playlists_count":0,"primary_email_confirmed":true,"locale":""};

    if(Users.find({id:userobj.id}).fetch().length === 0) //add
      Users.insert(userobj);
    else
      console.log('Hello '+userobj.id);
    if(!activesession){
      Session.setAuth('userobj',userobj);
      activesession = userobj;
    }
    Router.go('/home');
    // show user
    // if(views.length===0)
      // views.push(Blaze.render(Template.Home,document.getElementsByTagName('body')[0]));
    }

  function sclogin(){
    SC.initialize({
        client_id: clientid,
        redirect_uri: 'http://52.25.14.57:3000/_oauth/soundcloud'
    });
    SC.connect(function() {
      SC.get('/me', function(me) {
        loguser(me);
      });
    });
  }

  function getfollowers(){
    offset = 0;
    followers = [];
    var job = [];
    for(var i=0; i<Math.ceil((activesession.followers_count-offset)/50); i++)
      job.push(fetchfoll);
    job.push(function(cb){
      views.followers = Blaze.renderWithData(Template.followers,{followers:followers},$('.people')[0]);
      Session.set('followers',followers);
      // console.log(followers);
      cb(null);
    });
    async.series(job);
  }
  function fetchfoll(cb){
    $.get('http://api.soundcloud.com/users/'+activesession.id+'/followers.json?client_id='+clientid+'&offset='+offset,{},function(data){
      offset = offset+50;
      console.log(offset);
      if(followers === null)
        followers = data;
      else
        followers = followers.concat(data);
      cb(null,true);
    },'json').fail(function(){
      cb(true);
      alert('Some Error Occoured - Network - Followers Fetch Failed');
    });
  }
  function addthis(someid){
    var url = 'http://api.soundcloud.com/users/'+someid+'/tracks.json?client_id='+clientid;
    $.get(url,{},
      function(data){
          console.log(data);
          var totaltags = [];
          _.each(data,function(d){
              totaltags.push(_.compact(d.tag_list.trim().split('"')));
          });
          var _tags = Session.get('tags');
          //_tags = _tags.concat(totaltags);
          Session.set('tags',_.flatten(_tags));
    },'json').fail(function(){
      alert('Could not load this artist data');
    });
  }


  //  TEMPLATES


  Template.Home.helpers({
    avatar:function(){
      return activesession!=null ? activesession.avatar_url : '';
    },
    name:function(){
     return activesession!=null ?activesession.username : '';
    },
    logged:function(){
      return activesession===null ? true : false;
    },
    loading:function(){
      return Session.get('followers')===null ? true:false;
    },
    istag: function(){
        return Session.get('tags').length === 0 ? false : true;
    },
    tags:function(){
        return Session.get('tags');
    }
  });
  Template.Home.rendered = function(){
    if(!Session.get('followers'))
      getfollowers();
    else
      views.followers = Blaze.renderWithData(Template.followers,{followers:followers},$('.people')[0]);
  };
  Template.Login.events({
    'click .login img':function(){
      if(dev)
        loguser(null);
      else
          sclogin();
    }
  });
  Template.Login.helpers({
      islogged:function(){
          return (Session.get('userobj')||null)===null?true:false;
      }
  });
  Template.body.helpers({
    ishome:function(){
      return Router.current().route.path() === '/home' ? true : false;
    },
    isabout:function(){
      return Router.current().route.path() === '/about' ? true : false;
    }
  });
  Template.followers.events({
    'click ul li':function(){
      addthis(this.id);
    }
  });
}
if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    console.log('~jabdesign presents.');
  });


  //
  // ROUTES
  //
  Router.route('/_oauth/soundcloud',function(){

    this.response.statusCode = 200;
    this.response.end('<!DOCTYPE html>\
      <html lang="en">\
        <head>\
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\
          <title>Connect with SoundCloud</title>\
        </head>\
        <body onload="window.opener.setTimeout(window.opener.SC.connectCallback, 1)">\
          <b style="text-align: center;">This popup should automatically close in a few seconds</b>\
        </body>\
      </html>');
  },{where:'server'});

}
